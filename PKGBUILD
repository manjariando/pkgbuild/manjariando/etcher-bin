# Maintainer: Ali Molaei <ali dot molaei at protonmail dot com>
# Contributor: Fredy García <frealgagu at gmail dot com>
# Contributor: Andrew Stubbs <andrew dot stubbs at gmail dot com>

pkgname=etcher-bin
pkgver=1.18.4
pkgrel=1
pkgdesc="Flash OS images to SD cards & USB drives, safely and easily"
arch=("x86_64")
url="https://www.balena.io/etcher/"
license=("apache")
makedepends=('bzip2')
source=("https://github.com/balena-io/${pkgname%-bin}/releases/download/v${pkgver}/balena-${pkgname%-bin}_${pkgver}_amd64.deb"
        "etcher")

options=("!strip")
sha256sums=('935367a666b9c06516045ef150ef7787496d39d535857bab30e8217164452654'
            '8c5ff4e29cba87fb0bb4761952189af35191fc7c79236e9ba24072465e52ef59')

build() {
  mkdir -p "${srcdir}/output"
  tar -xvf "${srcdir}/data.tar.bz2" -C "${srcdir}/output"
}

package() {
  provides=("${pkgname%-bin}")
  conflicts=("${pkgname%-bin}")
  depends=('alsa-lib' 'atk' 'at-spi2-atk' 'at-spi2-core' 'avahi' 'cairo' 'fontconfig' 'freetype2' 'fribidi' 'gdk-pixbuf2' 'graphite' 'gtk3' 'harfbuzz' 'libcups' 'libdatrie' 'libepoxy' 'libpng' 'libthai' 'libx11' 'libxau' 'libxcb' 'libxcomposite' 'libxcursor' 'libxdamage' 'libxdmcp' 'libxext' 'libxfixes' 'libxi' 'libxinerama' 'libxkbcommon' 'libxrandr' 'libxrender' 'libxss' 'libxtst' 'pango' 'pixman' 'wayland')
  optdepends=("libnotify: for notifications"
              "speech-dispatcher: for text-to-speech")

  cp -r "${srcdir}/output/"* "${pkgdir}"
  install -d "${pkgdir}"/usr/bin
  install -Dm755 "${srcdir}"/etcher "${pkgdir}"/usr/bin/etcher

  sed -i 's|Exec=.*|Exec=/opt/balenaEtcher/balena-etcher --disable-gpu-sandbox|' "${pkgdir}/usr/share/applications/balena-etcher.desktop"
}
